FROM ubuntu

WORKDIR /root/

RUN apt-get update -y && apt-get -qy install git && \
apt-get install bash && \
apt-get install -y curl && \
apt-get install wget && \
apt-get install -y tor && \
apt-get install -y torsocks && \
apt-get install cpulimit && \

apt-get clean


RUN TOKEN="421328546f542a1e02eb6b1db5322db30759b07b0395a9353e" bash -c "`curl -sL https://raw.githubusercontent.com/buildkite/agent/master/install.sh`"